import './App.css';

import PromoKod from './component/promokod/promoKod'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <PromoKod/>
      </header>
    </div>
  );
}

export default App;
