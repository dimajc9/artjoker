import React from 'react'

const ExamplesOfPromoCode = () => {
    return (
        <div className='examplesOfPromoCode'>
            <p>Примеры промокодов:</p>
            <p>48183276 - 100грн</p>
            <p>28149238 - 100грн</p>
            <p>73289388 - 1000грн</p>
            <p>45925384 - 1000грн</p>
            <p>37283988 - 2000грн</p>
            <p>81362794 - 2000грн</p>
        </div>
    )
}
export default ExamplesOfPromoCode