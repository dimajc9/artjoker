import React, { useState } from 'react'

import ExamplesOfPromoCode from './examplesOfPromoCode'

const PromoKod = () => {
    const [number, setNumber] = useState(37283988)
    const [bonus, setBonus] = useState(0)
    const [output, setOutput] = useState([])
    const [sample, setSample] = useState(false)
    
    let sNumber = number.toString()
    for (let i = 0; i < sNumber.length; i++) {
        output.push(+sNumber.charAt(i));
    }

    const promoTwoThousand = (num) => {
        let odd = []
        let arg = []
        for(let i = 0; i < sNumber.length; i++){
            if(num[i] % 2){
                odd.push(num[i])
            } else {
                odd.push(undefined)
            }
        }
        for(let i = 0; i < odd.length; i++){    
            if(odd[i] % 2 && odd[i+1] % 2 && odd[i+2] == undefined){
                arg.push(odd[i], odd[i+1])
            }
            else if(odd[i] % 2 && odd[i+1] % 2 && odd[i+2] % 2){
                break
            }
        }
        if(arg[0] < arg[1] && arg[2] < arg[3]){
            setBonus((prevState) => prevState + 2000)
        }
        else{
            promoOneThousand(num)
        }
    }
    const promoOneThousand = (num) => {
        let odd = []
        let arg = 0
        for(let i = 0; i < sNumber.length; i++){
            if(num[i] % 2){
                odd.push(num[i])
            } else {
                odd.push('')
            }
        }
        for(let i = 0; i < odd.length; i++){    
            if(odd[i] % 2 && odd[i+1] % 2 && odd[i+2] == ''){
                arg += 1
            }else if(odd[i] % 2 && odd[i+1] % 2 && odd[i+2] % 2){
                break
            }
        }
        if(arg >= 2){
            setBonus((prevState) => prevState + 1000)
        } else {
            promoOneHundred(num)
        }
    }

    const promoOneHundred = (num) => {
        let uniq = [...new Set(num)]
        let even = 0
        let odd = 0
        for(let i = 0; i < sNumber.length; i++){
            if(uniq[i] !== undefined){
                if(uniq[i] % 2){
                    odd += uniq[i]
                }
                else{
                    even += uniq[i]
                }
            }
        }
        if(even > odd){
            setBonus((prevState) => prevState + 100)

        } else {
            setBonus((prevState) => prevState + 0)
        }
    }


    return(
        <div className='promoKod'>
            <h3>Бонусы: {bonus}грн</h3>
                <input type="text" step="10" maxLength="8" onChange={(e) => setNumber(e.target.value)} />
                <button type='submit' onClick={() => {promoTwoThousand(number)}}>Применить</button>
                <div>
                    <button onClick={() => {setSample((prevState) => !prevState)}}>Посмотреть Примеры</button>
                </div>
                {sample && <ExamplesOfPromoCode />}
        </div>
    )
}

export default PromoKod